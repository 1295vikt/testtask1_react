import axios from "axios";

let instance = axios.create({
  baseURL: process.env.REACT_APP_MAIN_API_URL,
  headers: {
    "Content-type": "application/json",
  },
});

instance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

const responseSuccessInterceptor = (response) => {
  return response.data;
};

const responseErrorInterceptor = (error) => {
  console.warn("Error status", error.response.status);

  console.log(error.response);
  return Promise.resolve({
    errorMessage: error.response.data,
  });
};

instance.interceptors.response.use(
  responseSuccessInterceptor,
  responseErrorInterceptor
);

export const http = instance;
