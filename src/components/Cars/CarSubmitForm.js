import React, { Component } from "react";

import "./Cars.scss";

import { Formik, Field, Form, ErrorMessage } from "formik";
import { withRouter } from "react-router-dom";

import * as Yup from "yup";

import CarDataService from "../../services/carService";
import CompanyDataService from "../../services/companyService";

import { Page404 } from "../Page404";
import { SelectField } from "../../FormikSelect";

import ClipLoader from "react-spinners/ClipLoader";

class CarSubmitForm extends Component {
  constructor(props) {
    super(props);
    this.submitCar = this.submitCar.bind(this);
    this.handleErrorMessage = this.handleErrorMessage.bind(this);
    this.retrieveCompanyDictionary = this.retrieveCompanyDictionary.bind(this);
    this.retrieveColorDictionary = this.retrieveColorDictionary.bind(this);


    this.state = {
      currentYear: new Date().getFullYear(),
      car: null,

      loading: true,
      processingRequest: false,
      isNotFound: false,

      errorMessage: "",
    };
  }

  async componentDidMount() {
    this.retrieveCompanyDictionary();
    this.retrieveColorDictionary();
    if (this.props.mode === "UPDATE") {
      var car = this.props.location.car ?? (await this.retrieveCar());

      this.setState({
        car: car,
        loading: false,
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  async retrieveCar() {
    const response = await CarDataService.get(this.props.match.params.id);

    if (response.hasOwnProperty("errorMessage")) {
      this.setState({
        isNotFound: true,
      });
      return;
    }

    return response;
  }

  async retrieveCompanyDictionary() {
    const response = await CompanyDataService.getDictionary();

    var companyOptions = [];
    response.map((company) =>
      companyOptions.push({ value: company.Key, label: company.Value })
    );
    this.setState({
        companySelection: companyOptions,
      });
  }

  async retrieveColorDictionary() {
    const response = await CarDataService.getColors();

    var colorOptions = [];
    response.map((color) =>
      colorOptions.push({ value: color.Key, label: color.Value })
    );

    this.setState({
      colorSelection: colorOptions,
    });
  }

  async submitCar(fields) {
    this.setState({
      errorMessage: "",
      processingRequest: true,
    });

    var request = {
      model: fields.model,
      companyId: fields.company,
      colorId: fields.color,
      bodyType: fields.bodyType,
      fuelType: fields.fuelType,
      manufactureYear: fields.manufactureYear,
      mileage: fields.mileage,
      driverName: fields.driverName,
      driverContactPhone: fields.driverContactPhone,
    };

    try {
      var response;

      console.log("fields", response);
      if (this.props.mode === "UPDATE") {
        response = await CarDataService.update(
          this.props.match.params.id,
          request
        );
      } else {
        response = await CarDataService.create(request);
      }

      if (response.errorMessage) {
        this.setState({ errorMessage: response.errorMessage });
        return;
      }

      const { history } = this.props;
      history.push("/cars/" + response.id);
    } finally {
      this.setState({
        processingRequest: false,
      });
    }
  }

  handleErrorMessage() {
    this.setState({ errorMessage: "" });
  }

  render() {
    const { car, currentYear } = this.state;

    return (
      <>
        <div className="ring-spinner">
          <ClipLoader
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>

        {!this.state.loading &&
          (!this.state.isNotFound ? (
            <Formik
              initialValues={
                car ?? {
                  model: "",
                  company: null,
                  color: null,
                  bodyType: "",
                  fuelType: "",
                  manufactureYear: 2000,
                  mileage: 0,
                  driverName: "",
                  driverContactPhone: "",
                }
              }
              validationSchema={Yup.object().shape({
                model: Yup.string()
                  .required("Model is required")
                  .max(30, "Model name is too long"),
                 company: Yup.number().required("Company is required"),
                color: Yup.number().required("Color is required"),
                bodyType: Yup.string().required("Body type is required"),
                fuelType: Yup.string().required("Fuel type required"),
                manufactureYear: Yup.number()
                  .required("Manufacture year is required")
                  .min(2000, "Manufacture year must be at least 2000")
                  .max(currentYear, "Manufacture year is invalid"),
                mileage: Yup.number()
                  .min(0, "Mileage is invalid")
                  .max(2000000, "Mileage is invalid"),
                driverName: Yup.string().max(50, "Driver name is too long"),
                driverContactPhone: Yup.string()
                  .required("Phone is required")
                  .matches(
                    /^\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})$/,
                    "Phone number is inavlid"
                  ),
              })}
              onSubmit={(fields) => {
                this.submitCar(fields);
              }}
            >
              {({ errors, status, touched }) => (
                <Form>
                  <div className="form-group">
                    <label htmlFor="model">Car Model</label>
                    <Field
                      name="model"
                      type="text"
                      className={
                        "form-control" +
                        (errors.model && touched.model ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="model"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="company">Company</label>
                    <Field
                      name="company"
                      component={SelectField}
                      options={this.state.companySelection}
                    />
                    <ErrorMessage
                      name="company"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="color">Color</label>
                    <Field
                      component={SelectField}
                      name="color"
                      options={this.state.colorSelection}
                      className={
                        errors.color && touched.color ? " is-invalid" : ""
                      }
                    />
                    <ErrorMessage
                      name="color"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="bodyType">Body Type</label>
                    <Field
                      name="bodyType"
                      type="text"
                      className={
                        "form-control" +
                        (errors.bodyType && touched.bodyType
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="bodyType"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="fuelType">Fuel Type</label>
                    <Field
                      name="fuelType"
                      type="text"
                      className={
                        "form-control" +
                        (errors.fuelType && touched.fuelType
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="fuelType"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="manufactureYear">Year of Manufacture</label>
                    <Field
                      name="manufactureYear"
                      type="number"
                      className={
                        "form-control" +
                        (errors.manufactureYear && touched.manufactureYear
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="manufactureYear"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="mileage">Mileage</label>
                    <Field
                      name="mileage"
                      type="number"
                      className={
                        "form-control" +
                        (errors.mileage && touched.mileage ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="mileage"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="driverName">Driver Name</label>
                    <Field
                      type="text"
                      name="driverName"
                      className={
                        "form-control" +
                        (errors.driverName && touched.driverName
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="driverName"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="driverContactPhone">
                      Driver Contact Phone
                    </label>
                    <Field
                      name="driverContactPhone"
                      type="text"
                      className={
                        "form-control" +
                        (errors.driverContactPhone && touched.driverContactPhone
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="driverContactPhone"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary mr-2">
                      Submit
                    </button>
                    <button type="reset" className="btn btn-secondary">
                      Reset
                    </button>

                    <span className="submit-spinner">
                      <ClipLoader
                        size={22}
                        color={"#123abc"}
                        loading={this.state.processingRequest}
                      />
                    </span>

                    <span className="response-error-message">
                      {this.state.errorMessage}
                    </span>
                  </div>
                </Form>
              )}
            </Formik>
          ) : (
            <Page404 />
          ))}
      </>
    );
  }
}

export default withRouter(CarSubmitForm);
