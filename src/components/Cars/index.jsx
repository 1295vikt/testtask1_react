import React, { Component } from "react";

import Select from "react-select";
import "./Cars.scss";

import CarDataService from "../../services/carService";
import CompanyDataService from "../../services/companyService";

import ClipLoader from "react-spinners/ClipLoader";

import ReactPaginate from "react-paginate";

import { Switch, Link, Route } from "react-router-dom";

import { SUBMIT_FORM_MODES } from "../../App";
import CarSubmitForm from "./CarSubmitForm";
import CarDetails from "./CarDetails";

const TITLE = "Cars";

export default class CarList extends Component {
  constructor(props) {
    super(props);
    this.retrieveCars = this.retrieveCars.bind(this);
    this.retrieveCompanyDictionary = this.retrieveCompanyDictionary.bind(this);
    this.handlePageClick = this.handlePageClick.bind(this);
    this.onCompanySelect = this.onCompanySelect.bind(this);

    this.state = {
      loading: true,

      companySelection: null,
      selectedCompany: null,
      selectedCompanyId: null,

      cars: null,

      pageCount: 1,
      perPage: 10,
      currentPage: 1,
    };
  }

  async componentDidMount() {
    this.retrieveCompanyDictionary();
    this.retrieveCars();
  }

  async retrieveCompanyDictionary() {
    const response = await CompanyDataService.getDictionary();

    var companyOptions = [{ value: null, label: "All" }];
    response.map((company) =>
      companyOptions.push({ value: company.Key, label: company.Value })
    );

    this.setState({
      companySelection: companyOptions,
    });
  }

  async retrieveCars() {
    this.setState({
      loading: true,
    });

    console.log("state", this.state);

    const response = await CarDataService.getPage(
      this.state.perPage,
      this.state.currentPage,
      this.state.selectedCompanyId
    );

    this.setState({
      pageCount: response.totalPages,
      cars: response.cars,

      loading: false,
    });
  }

  async handlePageClick(e) {
    await this.setState({
      currentPage: e.selected + 1,
    });

    this.retrieveCars();
  }

  async onCompanySelect(selectOption) {
    await this.setState({
      selectedCompany: selectOption,
      selectedCompanyId: selectOption.value,
      currentPage: 1,
    });

    this.retrieveCars();
  }

  loadingDisplayClass() {
    return this.state.loading ? "d-none" : "";
  }
  render() {
    const { cars } = this.state;

    return (
      <>
        <div className="ring-spinner">
          <ClipLoader
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>

        <div className="list">
          <div className={"row"}>
            <div className="col-lg-2">
              <h4>{TITLE}</h4>
            </div>
            <Select
              className="col-lg-3"
              options={this.state.companySelection}
              onChange={this.onCompanySelect}
              placeholder="Choose company..."
              value={this.state.selectedCompany}
            />
          </div>
          {!this.state.loading && (
            <>
              <div className="row">
                {cars.map((car, index) => (
                  <div className="col-lg-6" key={index}>
                    <Link to={"/cars/" + car.id} className="media media-car">
                      <img className="mr-3" src={car.imageURL} alt="" />
                      <div className="media-body">
                        <div>
                          <label>
                            <strong>Model:</strong>
                          </label>{" "}
                          {car.model}
                        </div>
                        <div>
                          <label>
                            <strong>Driver:</strong>
                          </label>{" "}
                          {car.driverName}
                        </div>
                        <div>
                          <label>
                            <strong>Color:</strong>
                          </label>{" "}
                          {car.color}
                        </div>
                        <div>
                          <label>
                            <strong>Company</strong>
                          </label>{" "}
                          {car.companyName}
                        </div>
                        <div>
                          <label>
                            <strong>Driver Phone:</strong>
                          </label>{" "}
                          {car.driverContactPhone}
                        </div>
                      </div>
                    </Link>
                  </div>
                ))}
                ;
              </div>

              <div className={"row"}>
                <ReactPaginate
                  previousLabel={"<<"}
                  nextLabel={">>"}
                  breakLabel={"..."}
                  breakClassName={"break-me"}
                  pageCount={this.state.pageCount}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={this.handlePageClick}
                  forcePage={this.state.currentPage - 1}
                  containerClassName={"pagination"}
                  subContainerClassName={"pages pagination"}
                  activeClassName={"active"}
                />
              </div>
            </>
          )}
        </div>
      </>
    );
  }
}

export const CarRouter = () => {
  return (
    <Switch>
      <Route exact path="/cars" component={CarList} />
      <Route
        path={["/cars/add"]}
        render={() => <CarSubmitForm mode={SUBMIT_FORM_MODES.CREATE} />}
      />
      {/* <Route
        path={"/companies/:id/edit"}
        render={() => (
          <CompanySubmitForm mode={SUBMIT_FORM_MODES.UPDATE} />
        )}
      /> */}
      <Route path={["/cars/:id"]} component={CarDetails} />
    </Switch>
  );
};
