import React, { Component } from "react";

import "./Cars.scss";

import CarDataService from "../../services/carService";

import { Page404 } from "../Page404";

import ClipLoader from "react-spinners/ClipLoader";
import { Table, Button } from "react-bootstrap";
import {Collapse} from 'react-collapse';

import { withRouter, Link } from "react-router-dom";

class CarDetails extends Component {
  constructor(props) {
    super(props);
    this.retrieveCarDetails = this.retrieveCarDetails.bind(this);
    this.deleteCar = this.deleteCar.bind(this);
    this.toggleImages = this.toggleImages.bind(this);

    this.state = {
      loading: false,
      car: null,
      showImages: false,
    };
  }

  async componentDidMount() {
    await this.retrieveCarDetails();
  }

  async retrieveCarDetails() {
    try {
      var id = this.props.match.params.id;

      this.setState({
        loading: true,
      });

      const response = await CarDataService.get(id);

      window.console.log(response.hasOwnProperty("errorMessage"));

      if (response.hasOwnProperty("errorMessage")) {
        return;
      }

      this.setState({
        car: response,
      });

      console.log(response);
    } finally {
      this.setState({
        loading: false,
      });
    }
  }

  async deleteCar(id) {
    await CarDataService.delete(id);

    const { history } = this.props;
    history.push("/cars");
  }

  toggleImages() {
    this.setState({
      showImages: !this.state.showImages,
    });
  }

  render() {
    const { car } = this.state;

    return (
      <>
        <div className="ring-spinner">
          <ClipLoader
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>

        {car ? (
          <div>
            <hr />
            <div>
              <label>
                <strong>Model:</strong>
              </label>{" "}
              {car.model}
            </div>
            <div>
              <label>
                <strong>Company:</strong>
              </label>{" "}
              {car.companyName}
            </div>
            <div>
              <label>
                <strong>Color:</strong>
              </label>{" "}
              {car.color}
            </div>
            <div>
              <label>
                <strong>Body Type:</strong>
              </label>{" "}
              {car.bodyType}
            </div>
            <div>
              <label>
                <strong>Fuel Type:</strong>
              </label>{" "}
              {car.fuelType}
            </div>
            <div>
              <label>
                <strong>Manufacture Year:</strong>
              </label>{" "}
              {car.manufactureYear}
            </div>
            <div>
              <label>
                <strong>Driver:</strong>
              </label>{" "}
              {car.driverName}
            </div>
            <div>
              <label>
                <strong>Driver Phone:</strong>
              </label>{" "}
              {car.driverContactPhone}
            </div>

            <div>
              <label>
                <strong>Images:</strong>
              </label>{" "}
              {car.images && car.images.length ? (
                <>
              <Button onClick={this.toggleImages.bind(this)} variant="light">{this.state.showImages ? "Hide" : "Show"}</Button>
                  <Collapse isOpened={this.state.showImages} initialStyle={{height: 0, overflow: 'hidden'}}>
                  {car.images.map((image, index) => (
                    <img
                      src={image.imageURL}
                      alt=""
                      key={"image-car-" + index}
                    ></img>
                  ))}
                  </Collapse>
                </>
              ) : (
                "No images available"
              )}
            </div>

            <div>
              <label>
                <strong>Damages:</strong>
              </label>{" "}
              <div>
                {car.damages && car.damages.length ? (
                  <Table
                    className="table"
                    responsive
                    striped
                    bordered
                  >
                    <thead>
                      <tr>
                        <th className="th-sm" scope="col">
                          Description
                        </th>
                        <th className="th-sm" scope="col">
                          Repair Cost
                        </th>
                        <th className="th-sm" scope="col">
                          Repaired
                        </th>
                        <th className="th-sm" scope="col">
                          Accident
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {car.damages.map((damage, index) => (
                        <tr key={index}>
                          <td>{damage.damageDescription}</td>
                          <td>
                            <center>{damage.estimatedRepairCostUSD}</center>
                          </td>
                          <td>
                            <center>
                              {damage.isRepaired ? (
                                <img src="/images/true_tick.svg" alt="" />
                              ) : (
                                <img src="/images/false_cross.svg" alt="" />
                              )}
                            </center>
                          </td>
                          <td>
                            {
                              <center>
                                <Link to={"/accidents/" + damage.accidentId}>
                                  {" "}
                                  <img
                                    className="btn-accident"
                                    src="/images/accident.svg"
                                    alt=""
                                  />{" "}
                                </Link>
                              </center>
                            }
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                ) : (
                  <div>The car recieved no damages</div>
                )}
              </div>
            </div>

            <div></div>
            <hr />
            <h4>
              <Link
                to={{
                  pathname: "/cars/" + car.id + "/edit",
                  car,
                }}
                className="badge badge-warning"
              >
                Edit
              </Link>
              <button
                onClick={this.deleteCar.bind(this,car.id)}
                className="badge badge-danger"
              >
                Delete
              </button>
            </h4>
          </div>
        ) : (
          <>{!this.state.loading && <Page404 />}</>
        )}
      </>
    );
  }
}

export default withRouter(CarDetails);
