import React from "react";
import { Link } from "react-router-dom";

export default class NavBar extends React.Component {
  componentDidUpdate() {
    window.console.log("Nav bar updated");
  }
  
  render() {
    return (
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <Link to="/home" className="navbar-brand">
          Home
        </Link>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/companies"} className="nav-link">
              Companies
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/cars"} className="nav-link">
              Cars
            </Link>
          </li>
        </div>
      </nav>
    );
  }
}
