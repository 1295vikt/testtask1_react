import React, { Component } from "react";

import { map } from "underscore";
import { Link } from "react-router-dom";

import { ReactComponent as CompanyIcon } from "../../images/company.svg";
import { ReactComponent as CarIcon } from "../../images/car.svg";
import { ReactComponent as AccidentIcon } from "../../images/accident.svg";

import "./Home.scss";

import { Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";

const SECTIONS = [
  { title: "Companies", href: "/companies", Icon: CompanyIcon },
  { title: "Cars", href: "/cars", Icon: CarIcon },
  { title: "Accidents", href: "/accidents", Icon: AccidentIcon },
];

export default class Home extends Component {
  render() {
    return (
      <Container fluid="md">

        <Row>
          {map(SECTIONS, ({ title, href, Icon }, index) => (
            <Col key={"home_section_" + index}>
              <Link className="Nav-Block" to={href}>
                <Icon className="Nav-Block-Icon" />
                <span className="Nav-Block-Title">{title}</span>
              </Link>
            </Col>
          ))}
        </Row>
      </Container>
    );
  }
}
