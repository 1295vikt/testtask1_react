import React, { Component } from "react";

import "./Companies.scss";

import { Formik, Field, Form, ErrorMessage } from "formik";
import { withRouter } from "react-router-dom";

import * as Yup from "yup";

import CompanyDataService from "../../services/companyService";

import { Page404 } from "../Page404";

import ClipLoader from "react-spinners/ClipLoader";

class CompanySubmitForm extends Component {
  constructor(props) {
    super(props);
    this.submitCompany = this.submitCompany.bind(this);
    this.handleErrorMessage = this.handleErrorMessage.bind(this);

    this.state = {
      company: null,

      loading: true,
      processingRequest: false,
      isNotFound: false,

      errorMessage: "",
    };
  }

  async componentDidMount() {
    if (this.props.mode === "UPDATE") {
      var company =
        this.props.location.company ?? (await this.retrieveCompany());

      this.setState({ company: company, loading: false });
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  async retrieveCompany() {
    const response = await CompanyDataService.get(this.props.match.params.id);

    if (response.hasOwnProperty("errorMessage")) {
      this.setState({
        isNotFound: true,
      });
      return;
    }

    return response;
  }

  async submitCompany(fields) {

    this.setState({
      errorMessage: "",
      processingRequest: true,
    });

    var request = {
      name: fields.name,
      country: fields.country,
      address: fields.address,
      contactPhone: fields.contactPhone,
      contactEmail: fields.contactEmail,
      comment: fields.comment,
      logoImageURL: fields.logoImageURL,
    };

    try{
      var response;

      if (this.props.mode === "UPDATE") {
        response = await CompanyDataService.update(
          this.props.match.params.id,
          request
        );
      } else {
        response = await CompanyDataService.create(request);
      }
  
      if (response.errorMessage) {
        this.setState({ errorMessage: response.errorMessage });
        return;
      }
  
      const { history } = this.props;
      history.push("/companies/" + response.id);
    }
    finally{
      this.setState({
        processingRequest: false,
      });
    } 

  }

  handleErrorMessage() {
    this.setState({ errorMessage: "" });
  }

  render() {
    const { company } = this.state;

    return (
      <>
        <div className="ring-spinner">
          <ClipLoader
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>

        {!this.state.loading &&
          (!this.state.isNotFound ? (
            <Formik
              initialValues={
                company ?? {
                  name: "",
                  country: "",
                  address: "",
                  contactPhone: "",
                  contactEmail: "",
                  comment: "",
                  logoImageURL: "",
                }
              }
              validationSchema={Yup.object().shape({
                name: Yup.string()
                  .required("Name is required")
                  .matches(/^[ A-Za-z]{2,30}$/, "Name is invalid"),
                country: Yup.string()
                  .required("Country is required")
                  .matches(/^[ A-Za-z]{3,30}$/, "Country name is invalid"),
                address: Yup.string()
                  .required("Address is required")
                  .max(250, "Address is too long"),
                contactPhone: Yup.string()
                  .required("Phone is required")
                  .matches(
                    /^\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})$/,
                    "Phone number is inavlid"
                  ),
                contactEmail: Yup.string()
                  .email("Email is invalid")
                  .required("Email is required"),
                comment: Yup.string().max(500, "Comment is too long"),
              })}
              onSubmit={(fields) => {
                this.submitCompany(fields);
              }}
            >
              {({ errors, status, touched }) => (
                <Form>
                  <div className="form-group">
                    <label htmlFor="name">Company Name</label>
                    <Field
                      name="name"
                      type="text"
                      className={
                        "form-control" +
                        (errors.name && touched.name ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="name"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="country">Country</label>
                    <Field
                      name="country"
                      type="text"
                      className={
                        "form-control" +
                        (errors.country && touched.country ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="country"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="address">Address</label>
                    <Field
                      name="address"
                      component="textarea"
                      className={
                        "form-control" +
                        (errors.address && touched.address ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="address"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="contactPhone">Contact Phone</label>
                    <Field
                      name="contactPhone"
                      type="text"
                      className={
                        "form-control" +
                        (errors.contactPhone && touched.contactPhone
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="contactPhone"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="contactEmail">Contact Email</label>
                    <Field
                      name="contactEmail"
                      type="email"
                      className={
                        "form-control" +
                        (errors.contactEmail && touched.contactEmail
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="contactEmail"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="comment">Comment</label>
                    <Field
                      component="textarea"
                      name="comment"
                      className={
                        "form-control" +
                        (errors.comment && touched.comment ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="comment"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="contactPhone">Logo Image Link</label>
                    <Field
                      name="logoImageURL"
                      type="text"
                      className={
                        "form-control" +
                        (errors.logoImageURL && touched.logoImageURL
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="logoImageURL"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary mr-2">
                      Submit
                    </button>
                    <button type="reset" className="btn btn-secondary">
                      Reset
                    </button>

                    <span className="submit-spinner">
                    <ClipLoader
                      size={22}
                      color={"#123abc"}
                      loading={this.state.processingRequest}
                    />
                    </span>

                      <span className="response-error-message">
                        {this.state.errorMessage}
                      </span>

                  </div>
                </Form>
              )}
            </Formik>
          ) : (
            <Page404 />
          ))}
      </>
    );
  }
}

export default withRouter(CompanySubmitForm);
