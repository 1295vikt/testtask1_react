import React, { Component } from "react";

import "./Companies.scss";

import CompanyDataService from "../../services/companyService";

import CompanyDetails from "./CompanyDetails";

import ClipLoader from "react-spinners/ClipLoader";

const TITLE = "Companies";

export default class CompanyOverview extends Component {
  constructor(props) {
    super(props);
    this.retrieveCompanies = this.retrieveCompanies.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveCompany = this.setActiveCompany.bind(this);

    this.state = {
      loading: true,
      companies: [],
      currentCompanyId: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveCompanies();
  }

  async retrieveCompanies() {
    this.setState({
      loading: true,
    });

    const response = await CompanyDataService.getAll();

    this.setState({
      loading: false,
      companies: response.data,
    });

    console.log(response.data);
  }

  refreshList() {
    this.retrieveCompanies();
    this.setState({
      currentCompany: null,
      currentIndex: -1,
    });
  }

  setActiveCompany(company, index) {
    this.setState({
      currentCompany: company,
      currentIndex: index,
    });
  }

  render() {
    const { companies, currentCompany, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>{TITLE}</h4>

          <center>
            <ClipLoader
              size={150}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </center>

          <ul className="list-group">
            {companies &&
              companies.map((company, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={this.setActiveCompany.bind(this, company, index)}
                  key={index}
                >
                  {company.name}
                </li>
              ))}
          </ul>
        </div>

        <div className="col-md-6">
          {currentCompany ? (
            <CompanyDetails
              companyId={
                this.state.currentCompany ? this.state.currentCompany.id : null
              }
            />
          ) : (
            <div>
              <br />
              <p>Click on company to show details...</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}

