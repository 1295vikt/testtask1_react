import React, { Component } from "react";

import "./Companies.scss";

import CompanyDataService from "../../services/companyService";

import { Page404 } from "../Page404";

import {Table,Button} from "react-bootstrap";
import ClipLoader from "react-spinners/ClipLoader";

import { withRouter, Link } from "react-router-dom";

class CompanyDetails extends Component {
  constructor(props) {
    super(props);
    this.retrieveCompanyDetails = this.retrieveCompanyDetails.bind(this);
    this.deleteCompany = this.deleteCompany.bind(this);

    this.state = {
      loading: false,
      company: null,
    };
  }

  async retrieveCompanyDetails() {
    try {
      var id = this.props.companyId ?? this.props.match.params.id;

      this.setState({
        loading: true,
      });

      const response = await CompanyDataService.get(id, true);

      window.console.log(response.hasOwnProperty("errorMessage"));

      if (response.hasOwnProperty("errorMessage")) {
        return;
      }

      this.setState({
        company: response,
      });
    } finally {
      this.setState({
        loading: false,
      });
    }
  }

  async deleteCompany(id) {
    await CompanyDataService.delete(id);

    const { history } = this.props;
    history.push("/companies");
  }

  async componentDidMount() {
    await this.retrieveCompanyDetails();
  }

  componentDidUpdate(prevProps) {
    if (this.props.companyId !== prevProps.companyId) {
      this.retrieveCompanyDetails();
    }
  }

  render() {
    const { company } = this.state;

    return (
      <>
        <div className="ring-spinner">
          <ClipLoader
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>

        {company ? (
          <div>
            <hr />
            <div>
              <h4>
              {company.name}
              </h4>            
            </div>
            <div>
              <label>
                <strong>Country:</strong>
              </label>{" "}
              {company.country}
            </div>
            <div>
              <label>
                <strong>Address:</strong>
              </label>{" "}
              {company.address}
            </div>
            <div>
              <label>
                <strong>Contact Phone:</strong>
              </label>{" "}
              {company.contactPhone}
            </div>
            <div>
              <label>
                <strong>Contact Email:</strong>
              </label>{" "}
              {company.contactEmail}
            </div>
            <div>
              <label>
                <strong>Comment:</strong>
              </label>{" "}
              <div>{company.comment}</div>
            </div>
            <div>
              <label>
                <strong>Cars:</strong>
              </label>
              {company.cars && company.cars.length ? (
                <Table
                  className="table"
                  responsive
                  striped
                  bordered
                >
                  <thead>
                    <tr>
                      <th className="th-sm" scope="col">
                        Model
                      </th>
                      <th className="th-sm" scope="col">
                        Color
                      </th>
                      <th className="th-sm" scope="col">
                        Driver Name
                      </th>
                      <th className="th-sm" scope="col">
                        Driver Phone
                      </th>
                      <th className="th-sm" scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {company.cars.map((car, index) => (
                      <tr key={index}>
                        <td>{car.model}</td>
                        <td>{car.color}</td>
                        <td>{car.driverName}</td>
                        <td>{car.driverContactPhone}</td>
                        <td>
                          <div className="table-button-block">
                            <Link to={"/cars/" + car.id}>
                              <img
                                className="btn-details"
                                src="/images/details.svg"
                                alt=""
                              />
                            </Link>{" "}
                            <Link
                              to={"/cars/" + car.id + "/edit"}
                              company={company}
                            >
                              {" "}
                              <img
                                className="btn-edit"
                                src="/images/pencil.svg"
                                alt=""
                              />
                            </Link>
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              ) : (
                <div>The company has no assigned cars yet</div>
              )}
            </div>
            <hr />
            <h4>
              <Link
                to={{
                  pathname: "/companies/" + company.id + "/edit",
                  company,
                }}
                className="badge badge-warning"
              >
                Edit
              </Link>
              <Button
                onClick={this.deleteCompany.bind(this, company.id)}
                className="badge badge-danger"
              >
                Delete
              </Button>
            </h4>
          </div>
        ) : (
          <>{!this.state.loading && <Page404 />}</>
        )}
      </>
    );
  }
}

export default withRouter(CompanyDetails);
