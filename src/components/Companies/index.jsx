import React, { Component } from "react";

import "./Companies.scss";

import CompanyDataService from "../../services/companyService";

import ClipLoader from "react-spinners/ClipLoader";

import { Switch, Link, Route } from "react-router-dom";

import { SUBMIT_FORM_MODES } from "../../App";
import CompanySubmitForm from "./CompanySubmitForm";
import CompanyDetails from "./CompanyDetails";
const TITLE = "Companies";

export default class CompanyList extends Component {
  constructor(props) {
    super(props);
    this.retrieveCompanies = this.retrieveCompanies.bind(this);

    this.state = {
      loading: true,
      companies: [],
    };
  }

  componentDidMount() {
    this.retrieveCompanies();
  }

  async retrieveCompanies() {
    this.setState({
      loading: true,
    });

    const response = await CompanyDataService.getAll();

    console.log(response);

    this.setState({
      loading: false,
      companies: response,
    });
  }

  render() {
    const { companies } = this.state;

    return (
      <>
        <div className="ring-spinner">
          <ClipLoader
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>
        {!this.state.loading && companies && (
          <div className="list row">
            <h4>{TITLE}</h4>
            <div className="row">
              {companies.map((company, index) => (
                <div className="col-lg-6" key={index}>
                  <Link to={"/companies/" + company.id} className="media media-company">
                    <img className="mr-3" src={company.logoImageURL} alt="" />
                    <div className="media-body">
                      <h5 className="mt-0">{company.name}</h5>

                      <div>
                        <label>
                          <strong>Country:</strong>
                        </label>{" "}
                        {company.country}
                      </div>
                      <div>
                        <label>
                          <strong>Phone:</strong>
                        </label>{" "}
                        {company.contactPhone}
                      </div>
                      <div>
                        <label>
                          <strong>Email:</strong>
                        </label>{" "}
                        {company.contactEmail}
                      </div>
                    </div>
                  </Link>
                </div>
              ))}
              <div className="col-lg-6">
                <Link to={"/companies/add"} className="media media-company">
                  <img className="plus-icon" src={"/images/add.svg"} alt="" />
                </Link>
              </div>
            </div>
          </div>
        )}
      </>
    );
  }
}

export const CompanyRouter = () => {
  return (
    <Switch>
      <Route exact path="/companies" component={CompanyList} />
      <Route
            path={["/companies/add"]}
            render={() => (
              <CompanySubmitForm mode={SUBMIT_FORM_MODES.CREATE} />
            )}
          />
      <Route
        path={"/companies/:id/edit"}
        render={() => (
          <CompanySubmitForm mode={SUBMIT_FORM_MODES.UPDATE} />
        )}
      />
      <Route path={["/companies/:id"]} component={CompanyDetails} />
    </Switch>
  );
};
