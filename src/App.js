import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";

import { Redirect } from "react-router";

import Home from "./components/Home/Home";
import { CompanyRouter } from "./components/Companies";
import { CarRouter } from "./components/Cars";
import NavBar from "./components/NavBar";
import { Page404 } from "./components/Page404";

export const SUBMIT_FORM_MODES = {
  CREATE: "CREATE",
  UPDATE: "UPDATE",
};

const App = () => {
  return (
    <Router>
      <NavBar />
      <div className="container mt-3">
        <Switch>
          <Route path={["/home"]} component={Home} />         
          <Route path={["/companies"]} component={CompanyRouter} />
          <Route path={["/cars"]} component={CarRouter} />
          <Redirect exact from="/" to="/home" />
          <Route component={Page404} />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
