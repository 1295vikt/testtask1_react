import {http} from "../http-common";

class CarDataService {
  getAll() {
    return http.get("/cars");
  }

  get(id) {
    return http.get(`/cars/${id}`);
  }

  getPage(itemsPerPage,currentPage,companyId) {
    return http.get(`/cars/page`, {
      params: {
        itemsPerPage: itemsPerPage,
        currentPage: currentPage,
        companyId: companyId
      }
    });
  }

  getColors() {
    return http.get("/cars/colors");
  }

  create(data) {
    return http.post("/cars", data);
  }

  update(id, data) {
    return http.put(`/cars/${id}`, data);
  }

  delete(id) {
    return http.delete(`/cars/${id}`);
  }

}

export default new CarDataService();