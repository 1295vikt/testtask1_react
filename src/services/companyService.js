import { http } from "../http-common";

class CompanyDataService {
  getAll() {
    return http.get("/companies");
  }

  get(id, includeCars) {
    return http.get(`/companies/${id}`, {
      params: { includeCars: includeCars },
    });
  }

  getDictionary() {
    return http.get("/companies/dictionary");
  }

  create(data) {
    return http.post("/companies", data);
  }

  update(id, data) {
    return http.put(`/companies/${id}`, data);
  }

  delete(id) {
    return http.delete(`/companies/${id}`);
  }
}

export default new CompanyDataService();
