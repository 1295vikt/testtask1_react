import React from "react";
import Select from "react-select";
import {useField} from 'formik';

export const SelectField = (props) => {
  const [field, state, {setValue, setTouched}] = useField(props.field.name);

  const onChange = ({value}) => {
    setValue(value);
    setTouched(true);
  };

  return (
    <Select {...props} onChange={onChange} onBlur={setTouched}/>
  );
}